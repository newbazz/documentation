# Starting your own instance

!!! note
    If you want to setup a development instance,
    please refer to [Deploying GitMate](../Developers/Deploying GitMate).

The purpose of this document is to guide you to setting up GitMate on your own
server for production purposes.

## 0. Prepare

You need to setup the following:

1. Domain for your GitMate instance (ex. gitmate.organization.tld).
2. GNU/Linux server with Docker and docker-compose installed.
3. SSL certificate for the domain (wildcard is ok)

## 1. Fetch the `example-setup`

We've prepared an example `docker-compose` setup with everything that GitMate
needs to operate, it's made for a typical single host (one domain) setup whether
it'll be operating in your organization's intranet or publicly facing isn't a
problem.

If you have `git` installed on your server, you can clone the repository like
usual:

```bash
git clone https://gitlab.com/gitmate/open-source/example-setup
```

If you don't have `git` installed, you can fetch the tarball and extract it:

```bash
curl https://gitlab.com/gitmate/open-source/example-setup/repository/master/archive.tar.gz -O gitmate.tar.gz
mkdir gitmate
tar xf gitmate.tar-gz --strip 1 -C gitmate
```

## 2. Fetching the required images

GitMate requires a number of Docker images which are used momentarily, You can
do this by running the following commands:

```bash
docker pull registry.gitlab.com/gitmate/open-source/coala-incremental-results:release
docker pull registry.gitlab.com/gitmate/open-source/result-bouncer:release
```

## 3. Replace the placeholders

We provide an easy script that replaces all of the placeholder text for the
configuration, You can execute it by running the following command:

```bash
./setup.sh
```

A routine of prompts will ask you to replace the placeholder with another piece
of text.

!!! note
    It's recommended to use a password generator like `pwgen` to generate the
    passwords for the database, message broker, etc.

## 4. Customize your setup

The example we provide is pretty basic and generalized to a typical four-core
production server. We recommend you to check every single file, familiarize with
the setup, and modify them if needed.

!!! note
    It's recommend checking `gitmate.environment` because it holds the
    configuration for the GitMate's backend.

## 5. Deploy

After you've finished customizing your setup to your needs and work environment,
you can deploy the instance by running the following command:

```bash
docker-compose up -d
```

The setup will deploy itself and startup, if anything goes wrong you can check
the logs by running the following command:

```bash
docker-compose logs
```

## Further reading

* [Using Docker in Docker](../Admins/Using_Docker_in_Docker)
